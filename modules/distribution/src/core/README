Entgra IoT Server @product.version@
==============================================================
Welcome to the Entgra IoT Server (IoTS) @product.version@ release.

Entgra IoT Server (IoTS) provides the essential capabilities required to implement a scalable server-side IoT Platform. These capabilities
involve device management, API/App management for devices, analytics, customizable web portals, transport extensions for MQTT, XMPP
and much more. Entgra IoTS contains sample device agent implementations for well-known development boards, such as Arduino UNO, Raspberry Pi,
Android and Virtual agents that demonstrate various capabilities. Furthermore, Entgra IoTS Community Edition is released under
the Apache Software License Version 2.0, one of the most business-friendly licenses available today.


Key Features
==================================
See the online Entgra IoT documentation for more information on product features:
https://entgra-documentation.gitlab.io/v@product.doc.version@


Installation & Running
==================================

Running the IoT server
==================================
1. Extract entgra-iot-@product.version@.zip and go to the extracted directory/bin.
2. Run iot-server.sh or iot-server.bat.
3. Point your favourite browser to  https://localhost:9443/devicemgt in order to see the available device types and operations.
4. Use the following username and password to login
   username : admin
   password : admin
5. Navigate to https://localhost:9443/api-store to see the available device APIs. You can subscribe to these APIs with the default application (or by creating a new application).
   In the API Store, go to my subscriptions and locate the client ID and secret, which can be used to invoke these APIs.
   

Running all runtimes (IoT, Analytics, Broker)
==================================================================

1. Extract entgra-iot-@product.version@.zip and go to the extracted directory/bin.
2. Run broker.sh (or broker.bat), then the iot-server.sh (iot-server.bat) and finally analytics.sh (or analytics.bat).
3. Access appropriate url for the related runtime. (For example, use  https://localhost:9443/devicemgt for the IoT Server runtime)


System Requirements
==================================

1. Minimum memory - 4GB
2. The portal app requires full Javascript enablement on the Web browser


Entgra IoT distribution directory
=============================================

 - bin
	  Contains various scripts .sh & .bat scripts

    - database
	  Contains the database

    - dbscripts
	  Contains all the database scripts

    - lib
	  Contains the basic set of libraries required to startup IoT Server
	  in standalone mode

    - repository
	  The repository where services and modules deployed in Entgra IoT.
	  are stored.

	- conf
	  Contains configuration files specific to IoT.

	- logs
	  Contains all log files created during execution of IoT.

    - resources
	  Contains additional resources that may be required, including sample
	  configuration and sample resources

    - samples
	  Contains some sample services and client applications that demonstrate
	  the functionality and capabilities of Entgra IoT.

    - tmp
	  Used for storing temporary files, and is pointed to by the
	  java.io.tmpdir System property

    - LICENSE.txt
	  Apache License 2.0 and the relevant other licenses under which
	  Entgra IoT is distributed.

    - README.txt
	  This document.

    - release-notes.html
	  Release information for Entgra IoT @product.version@

	- patches
	  Used to add patches related for all runtimes.

	-dropins
	  Used to add external jars(dependencies) of all runtimes.

	-extensions
	  Used to add carbon extensions.

	-servicepacks
	 Used to add service packs related to all runtimes.

	-webapp-mode

	-Entgra/components
	 Contains profiles for all runtimes and the plugins folder

	-Entgra/lib
	  Contains jars that are required/shared by all runtimes.

	-Entgra/analytics
	  Contains analytics runtime related files/folders.

	-Entgra/analytics/conf
	  Analytics runtime specific configuration files.

    -Entgra/analytics/repository
	  Where deployments of Analytics runtime is stored.

   -Entgra/broker
      Contains broker runtime related files/folders.

   -Entgra/broker/conf
      Broker runtime specific configuration files.

   -Entgra/broker/repository
      Where deployments of broker runtime is stored.


Secure sensitive information in Carbon configuration files
----------------------------------------------------------

There is sensitive information such as passwords in the Carbon configuration.
You can secure them by using secure vault. Please go through the following steps to
secure them with the default mode.

1. Configure secure vault with the default configurations by running the ciphertool
  script from the bin directory.

> ciphertool.sh -Dconfigure   (in UNIX)

This script automates the following configurations that you would normally need to do manually.

(i) Replaces sensitive elements in configuration files that have been defined in
     cipher-tool.properties, with alias token values.
(ii) Encrypts the plain text password which is defined in the cipher-text.properties file.
(iii) Updates the secret-conf.properties file with the default keystore and callback class.

cipher-tool.properties, cipher-text.properties and secret-conf.properties files
      can be found in the <IoT_HOME>/conf/security directory.

2. Start the server by running the iotserver script, which is in the <IoT_HOME>/bin directory.

> iotserver.sh   (in UNIX)

When running the default mode, it asks you to enter the master password
(By default, the master password is the password of the Carbon keystore and private key)

3. Change any password by running the ciphertool script, which is in the <IoT_HOME>/bin directory.

> ciphertool -Dchange  (in UNIX)

For more information, see
https://docs.wso2.com/display/ADMIN44x/Carbon+Secure+Vault+Implementation

Training
--------

Entgra (Pvt) Ltd. offers a variety of professional Training Programs for Entgra products.
For additional support on training information please goto https://entgra.io/contact


Support
-------

We are committed to ensuring that your enterprise middleware deployment is completely supported from evaluation to production. Our unique approach ensures that all support leverages our open development methodology and is provided by the very same engineers who build the technology.

For additional support information please refer to https://entgra.io/contact

---------------------------------------------------------------------------
(c) Copyright 2020 Entgra (Pvt) Ltd.
